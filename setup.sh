export PROJDIR=/tmp/chinese_nlp
export JAVA_HOME=""

mkdir -p $PROJDIR
cd $PROJDIR

# known good version: a4bd4c423c2d03966a81c0542310bcf6005ac272
git clone https://github.com/stanfordnlp/CoreNLP.git
cd CoreNLP
ant || ( echo "FAIL! Apache ant must be installed and available" && exit -1 )

cd $PROJDIR
git clone git@bitbucket.org:dan7tam/corenlp-python.git

cd $PROJDIR
# check for latest models: http://www-nlp.stanford.edu/software/corenlp.shtml
FILE=stanford-corenlp-full-2014-06-16.zip
curl http://www-nlp.stanford.edu/software/$FILE -O
unzip $FILE
rm $FILE
cd $(echo $FILE | sed 's/.zip//g')
curl http://www-nlp.stanford.edu/software/stanford-chinese-corenlp-2014-02-24-models.jar -O

# check that stanford-corenlp is set up properly

cd $PROJDIR
# java -Xmx3g -cp stanford-corenlp-full-2014-06-16/stanford-corenlp-3.4.jar:stanford-corenlp-full-2014-06-16/stanford-corenlp-3.4-models.jar:stanford-corenlp-full-2014-06-16/stanford-chinese-corenlp-2014-02-24-models.jar:stanford-corenlp-full-2014-06-16/xom.jar:stanford-corenlp-full-2014-06-16/joda-time.jar:stanford-corenlp-full-2014-06-16/jollyday.jar:stanford-corenlp-full-2014-06-16/ejml-0.23.jar edu.stanford.nlp.pipeline.StanfordCoreNLP -props CoreNLP/src/edu/stanford/nlp/pipeline/StanfordCoreNLP-chinese.properties

# python server
python corenlp-python/corenlp/corenlp.py --properties CoreNLP/src/edu/stanford/nlp/pipeline/StanfordCoreNLP-chinese.properties

# corepy
# python webpy_client.py 8099
# point browser to localhost:8099
