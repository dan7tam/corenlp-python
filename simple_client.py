#!/usr/bin/python

import argparse
import logging
import jsonrpclib
import json
import sys

def get_args():
    parser = argparse.ArgumentParser(description='Simple client for NLP server.')
    parser.add_argument('--server', default='localhost',
                       help='host name of server, default: localhost')
    parser.add_argument('--port', type=int, default=8080,
                       help='port number of server, default:8080')
    parser.add_argument('--query', help='query string')
    parser.add_argument('--filters', help="""fields and values to filter by. values should be
                        separated by commas, fields semi-colon, colon between fields and values,
                        for example: field1:value1;field2:value2,value3""")
    args = parser.parse_args()
    if not check_args(args):
        parser.print_help()
        sys.exit(1)
    return args

def check_args(args):
    return args.server and args.port and args.query

class Client:
    def __init__(self, server, port):
        self.server = jsonrpclib.Server(
            "http://{0}:{1}".format(server, port))

    def query(self, text, filters=None):
        result = json.loads(self.server.parse(text))
        if filters:
            return self.get_filtered(result, filters)
        else:
            return self.get_full(result)

    def debug_filter_query(self, text, filters):
        result = json.loads(self.server.parse(text))
        return self.get_filtered(result, filters), self.get_full(result)

    def get_filtered(self, result, filters):
        words = []
        filter_map = {}
        for filter in filters.split(';'):
            kv = filter.split(':')
            filter_map[kv[0]] = kv[1].split(',')

        for sentence in result['sentences']:
            for word in sentence['words']:
                for field in word[1].keys():
                    if field in filter_map.keys():
                        tag = word[1][field]
                        if tag in filter_map[field]:
                            words.append(u"{0}:{1}:{2}".format(field, tag, word[0]))
        return '\n'.join(words)

    def get_full(self, result):
        return json.dumps(result, sort_keys=True, indent=4,
                           ensure_ascii=False).encode('utf8')

if __name__  == '__main__':
    args = get_args()
    client = Client(args.server, args.port)
    print client.query(args.query, args.filters)

