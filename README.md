# A Python wrapper for the Java Stanford Core NLP tools
---------------------------

This is a fork of Torotoki's [fork](https://bitbucket.org/torotoki/corenlp-python) of Dustin Smith's [stanford-corenlp-python](https://github.com/dasmith/stanford-corenlp-python), a Python interface to [Stanford CoreNLP](http://nlp.stanford.edu/software/corenlp.shtml). It can either use as python package, or run as a JSON-RPC server.


## Download and Usage

1. install ant
2. sudo pip install pexpect jsonrpclib webpy
3. ./setup.sh

## Developer
   * Dan Tam [dantam@gmail.com]
