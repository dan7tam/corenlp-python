import web
from web import form

import jsonrpclib
import json

from simple_client import Client

render = web.template.render('webpy_templates/')

urls = (
  '/', 'index'
)

app = web.application(urls, globals())

web.config.debug = True

input_form = form.Form(
    form.Textarea('input', description="Input", rows=5, cols=80)
)

output_form = form.Form(
    form.Textarea('input', description="Input", rows=5, cols=80),
    form.Textarea('nouns', description="Specials", rows=10, cols=80),
    form.Textarea('output', description="Output", rows=50, cols=80)
)

client = Client('localhost', 8080)
filters="NamedEntityTag:PERSON;PartOfSpeech:NR"

class index:
    def GET(self):
        form = input_form()
        return render.simple_form(form)

    def POST(self):
        form = input_form()
        if not form.validates():
            return render.simple_form(form)
        else:
            nouns, debug = client.debug_filter_query(form.d.input, filters)
            outform = output_form()
            outform.input.value = form.d.input
            outform.output.value = debug
            outform.nouns.value = nouns
            return render.simple_form(outform)

if __name__ == "__main__":
    app.run()

